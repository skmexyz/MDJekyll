---
layout: page
title: About
permalink: /about/
---

## Introduction
This is a minimalistic Material Design Theme for Jekyll.
It has been forked from the [material-jekyll-theme](https://github.com/alexcarpenter/material-jekyll-theme) by [Alex Carpenter](http://alexcarpenter.me/).
I shall be adding in improvements on top of the already existing design.

## Configuration
Add the following to your `_config.yml`.

`fixedNav: 'true' #true or false`

`theme: purple # green, blue, orange, purple, grey`

## Roadmap

1. SEO

  1.1 Facebook OpenGraph

  1.2 Twitter Cards

  1.3 Google+ Meta Tags

  1.4 Google Analytics

  1.5 Bing Analytics

2. Accelerated Mobile Pages

3. Facebook Instant Articles
